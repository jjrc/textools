#!/bin/sh
# textools - tools for tex environment.
# texbib - compile tex along with aux bibliography and export to pdf.

filename=$1

if [ -z $filename ] || [[ $filename != *.tex  ]]; then
	echo "ERROR: Select a tex file."
	exit 1
fi

pdflatex $filename
bibtex $(echo $filename | sed s/tex/aux/)
pdflatex $filename
pdflatex $filename
