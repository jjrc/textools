# textools - some tools for tex environment. 
# Makefile.

NAME=textools
PREFIX=/usr/local
VERSION=0.2

options:
	@echo no compilation is needed:
	@echo - run \"make install\" to install $(NAME)
	@echo - run \"make uninstall\" to uninstall $(NAME)

install: config.sh getbibtex.sh texbib.sh
	mkdir -p $(PREFIX)/etc/$(NAME)
	mkdir -p $(PREFIX)/bin
	cp -f config.sh $(PREFIX)/etc/$(NAME)/config
	cp -f getbibtex.sh $(PREFIX)/bin/getbibtex
	cp -f texbib.sh $(PREFIX)/bin/texbib
	chmod 644 $(PREFIX)/etc/$(NAME)/config
	chmod 755 $(PREFIX)/bin/getbibtex
	chmod 755 $(PREFIX)/bin/texbib
	@echo
	@echo $(NAME) \(version $(VERSION)\) installed.

uninstall:
	rm -f $(PREFIX)/bin/getbibtex
	rm -f $(PREFIX)/bin/texbib
	rm -rf $(PREFIX)/etc/$(NAME)
	@echo
	@echo $(NAME) \(version $(VERSION)\) uninstalled.

.PHONY: options install uninstall
