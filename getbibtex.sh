#!/bin/sh
# textools - tools for tex environment.
# getbibtex - get references in bibtex format by providing a doi.

# - Example:
#   getbibtex 10.1038/s41586-020-2469-4

# Source config file.
[ -f "/usr/local/etc/textools/config" ] && . /usr/local/etc/textools/config
[ -f "$HOME/.config/textools/config" ] && . $HOME/.config/textools/config

# Message asking for DOI.
[ -z $1 ] && echo -e "Please, insert DOI.\n" && exit
doi=$(echo $1 | tr '[:upper:]' '[:lower:]')

# Get bibliography (if it exists).
bib_raw=$(curl -sLH "Accept: text/bibliography; style=bibtex" "https://dx.doi.org/$doi")

# Get error message if doesn't. If it does, the following variable is empty.
errormess=$(echo $bib_raw | grep \<title\> | sed s/.*\<title\>// | sed s/\<\\/title\>.*//)
[ "$errormess" == "Error: DOI Not Found" -o "$errormess" == "HTTP Status 400 – Bad Request" -o -z "$bib_raw" ] && echo -e "Error: DOI not found in the Crossref database!\n" && exit

# Display reference on screen.
echo "Reference to be added:"  && echo $bib_raw | sed "s/^\s//; s/\\},/\\},\\n/g; s/,/,\\n/; s/$/\\n/" | sed "s/^/  /g"

# Check if a bibliography file exists. If it doesn't, create it.
[ -f "$bib_file" ] || (echo -e "Bibliography file not found.\nA new bibliography file has been created.\n" && bib_file="bibfile.bib" && touch "$bib_file")

# The reference may exist and be already saved. Check for duplicate.
saved_doi=$(grep $doi "$bib_file")
[ -n "$saved_doi" ] && echo -e "This reference was previously added!\n" && exit

# Another reference may exist with the same identifier (e.g., the same first author has published another paper in the same year). Check for duplicate.
abc=("a" "b" "c" "d" "e" "f" "g" "h" "i" "j" "k" "l" "m" "n" "o" "p" "q" "r" "s" "t" "u" "v" "w" "x" "y" "z")
candidate_id=$(echo $bib_raw | awk -v FS="({|,)" '{print $2}')
coinc_number=$(grep $candidate_id "$bib_file" | wc -l)
new_id_def=${candidate_id}_${abc[$coinc_number]}

# If duplicate exists, choose another identifier.
[ $coinc_number -ne 0 ] && echo -e "$coinc_number partial and/or full coincidence(s) found for the identifier \"$candidate_id\"!" \
&& read -p "Please, choose another id (leave blank for default \"$new_id_def\"): " new_id \
&& echo && [ -z "$new_id" ] && new_id=$new_id_def
[ -n "$new_id" ] && bib_raw=$(echo $bib_raw | sed s/$candidate_id/$new_id/)

# Write reference if it is new and there are no errors.
echo $bib_raw | sed "s/^\s//; s/\\},/\\},\\n/g; s/,/,\\n/; s/$/\\n/"  >> "$bib_file" && echo -e "Reference successfully added!\n"

# Save backup file.
#bakfile=$(echo "$bib_file" | awk -F '/' '{print $NF}')
#bakpath=$(echo "$bib_file" | sed s/$bakfile//).bakfiles
#mkdir -p "$bakpath"
#nfiles=$(ls "$bakpath" | wc -l)
#cp "$bib_file" "$bakpath"/${bakfile}_v$(($nfiles+1)).bak
